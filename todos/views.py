from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "list.html", context)


def todo_list_detail(request, id):
    items = TodoItem.objects.filter(list=id)
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list": list,
        "items": items,
    }
    return render(request, "items.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
        context = {
            "form": form,
        }
    return render(request, "create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)
        context = {
            "form": form,
        }
    return render(request, "update.html", context)


def todo_list_delete(request, id):
    instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        instance.delete()
        return redirect("todo_list_list")
    return render(request, "delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
        context = {
            "form": form,
        }
    return render(request, "create_item.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    item = todo_item
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
        context = {
            "form": form,
        }
    return render(request, "update_item.html", context)
